$(document).ready(function() {

    $('.updateButton').on('click', function() {

        $.ajax({
            url : '/update_net_value',
            type : 'GET',
            success : function(response){
                var json = response;
                $('tbody').empty();
                console.log(json);
                for (x in json) {
                    tr = $('<tr>');
                    tr.append("<td>" + x + "</td>");
                    tr.append("<td>" + json[x] + "</td>");
                    tr.append('</tr>');
                    $('tbody').append(tr);
                }
            }
        });
    });

    // Attach onclick to add trade button
});
