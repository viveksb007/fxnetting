import mysql.connector


def initialize_db():
    # connect to mysql
    my_db = mysql.connector.connect(host="localhost", user="root", passwd="fxnetting")
    my_cursor = my_db.cursor(buffered=True)

    my_cursor.execute("use fxnetting;")
    return my_cursor, my_db
