import hashlib
import json
import os
import random
from sortedcontainers import SortedDict

from flask import Flask, request, render_template, session, jsonify
from flask_session import Session

from database_connection import initialize_db

app = Flask(__name__, static_url_path="/static")

app.config['SESSION_TYPE'] = 'filesystem'

app.secret_key = 'fxnetting'

app.config.from_object(__name__)
Session(app)
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


@app.route('/')
def index():
    return render_template("login.html")


@app.route('/trade_book')
def trade_book():
    return render_template("index.html")


@app.route('/add_trade', methods=['POST'])
def add_trade():
    client_code = request.json.get('client_code')
    currency_pair = request.json.get('currency_pair')
    base_direct = request.json.get('base_direct')
    base_notional = request.json.get('base_notional')
    trade_type = request.json.get('trade_type')
    trade_date = request.json.get('trade_date')
    value_date = request.json.get('value_date')
    price = request.json.get('price')
    quote_ccy_qty = base_notional * price

    trade_status = True
    if random.randint(0, 100) > 95:
        trade_status = False

    search_client_name_query = "SELECT client_name FROM client WHERE client_code = %s;"
    my_cursor.execute(search_client_name_query, [client_code])
    row = my_cursor.fetchall()
    client_name = row[0]

    query = "INSERT into trades VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    my_cursor.execute(query, [client_code, str(client_name), currency_pair,
                              base_notional, base_direct, price, trade_type, quote_ccy_qty,
                              trade_date, value_date])

    currency_data = get_value_from_db()
    currency_data = SortedDict(currency_data)
    return jsonify(currency_data)
    #return "trade executed successfully"
    
@app.route('/update_net_value', methods=['GET'])
def net_by_currency():
    currency_data = get_value_from_db()
    currency_data = SortedDict(currency_data)
    return jsonify(currency_data)

@app.route('/handle_login', methods=['POST'])
def handle_login():
    # receive post response
    username = request.form['userid']
    pwd = request.form['password']

    # calculate hash of password
    pwd = hashlib.md5(str(pwd).encode('utf-8')).hexdigest()

    # check credentials
    my_cursor.execute("SELECT COUNT(1) FROM employee WHERE username = %s;", [username])  # CHECKS IF USERNAME EXSIST
    if my_cursor.fetchone()[0]:
        my_cursor.execute("SELECT password FROM employee WHERE username = %s;", [username])
        password = (str(my_cursor.fetchone()[0]))

        if pwd == password:
            print("Login Successful")

            session["uname"] = str(username)
            currency_data = get_value_from_db()
            currency_data = SortedDict(currency_data)
            return render_template("index.html", currency_data=currency_data, username=username)

    else:
        # prompt user doesn't exist
        pass


def get_value_from_db():
    query = "SELECT ccy_pair, SUM(CASE " \
            "base_direction " \
            "WHEN 'B' THEN quote_ccy_qty " \
            "WHEN 'S' THEN -quote_ccy_qty END) " \
            "FROM trades GROUP BY ccy_pair order by ccy_pair asc;"
    my_cursor.execute(query)
    rows = my_cursor.fetchall()
    currency_data = {}
    for row in rows:
        currency_data[row[0]] = row[1]
    return currency_data


@app.route('/test')
def test():

    currency_data = {}
    return render_template("index.html", currency_data=currency_data)


if __name__ == '__main__':
    my_cursor, my_db = initialize_db()
    port = int(os.environ.get("PORT", 5000))
    app.run(host='localhost', port=port)
    my_cursor.close()
    my_db.close()
